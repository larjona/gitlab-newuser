# Settings file for gitlab_self_service

# Base URL for the GitLab instance. Must be HTTPS for OAuth2 to work
GITLAB_BASE_URL = 'https://gitlab.olasd.eu:10443/'

# Impersonation token for an administrator of the GitLab instance
# See https://<gitlab_instance>/admin/users/<admin_username>/impersonation_tokens
GITLAB_API_TOKEN = 'Sd9Kchs9kHK8cPeaLGbA'

# OAuth application credentials. Register the application on
# https://<gitlab_instance>/admin/applications with the following settings:
#
# redirect_uri: https://<self_service_domain>/login/authorized
# trusted: yes (avoids GitLab prompt for app authorization when logging a user in)
# scopes:
#  - read_user
#
GITLAB_OAUTH_CONSUMER_KEY = '5afcfeb25e4aba1e1da9fa3111b4d344107444d1a6a6558627f8181a85abbeb8'
GITLAB_OAUTH_CONSUMER_SECRET = '260f825041422c069e7a645598082a3723f3975e48d393760d7c1d8b8769f8bc'

# Extra parameters to pass to the user creation API
# All parameters listed on:
# https://docs.gitlab.com/ce/api/users.html#user-creation
#
GITLAB_USER_EXTRA_PARAMS = {
    'admin': False,
    'can_create_group': False,
    'external': True,
    # Useful for tests to avoid spam; switch to False on prod...
    'skip_confirmation': True,
}

# Extra parameters to pass to the group creation API
# All parameters listed on:
# https://docs.gitlab.com/ce/api/groups.html#new-group
#
GITLAB_GROUP_EXTRA_PARAMS = {
    'visibility': 'public',
    'request_access_enabled': True,
}

# CA bundle used for validation of the HTTPS connections to the GitLab API and
# OAuth2 provider
CA_BUNDLE = '/etc/ssl/certs/ca-certificates.crt'

# Flask secret key, used for CSRF protection and sessions
# For production, set to a static value, generated with os.urandom(24)
SECRET_KEY = b'\xe3\x0f\x1a\xcb\x94LAj\x81\x07\x9c\x7fl\xa6\xc15\x02!\x99oo\x84a)'
